FROM ubuntu:latest
LABEL maintainer="vwangsf@gmail.com"

RUN apt update
RUN apt install -y git build-essential autoconf automake pkg-config python3-pip libtool
RUN pip3 install --user future lxml
RUN git clone --recursive https://github.com/intel/mavlink-router

WORKDIR mavlink-router
RUN ./autogen.sh && ./configure CFLAGS='-g -O2' \
        --sysconfdir=/etc --localstatedir=/var --libdir=/usr/lib64 \
    --prefix=/usr --disable-systemd
RUN make
RUN make install

RUN mkdir /etc/mavlink-router
COPY files/mavlink-router.conf /etc/mavlink-router/main.conf

CMD mavlink-routerd -e 127.0.0.1:14551 -e 127.0.0.1:14552 -e 127.0.0.1:14553 -e 127.0.0.1:14554 -e 127.0.0.1:14555 $LISTEN
